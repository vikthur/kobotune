<?php

namespace kobotune;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Playlist extends Model
{
    public function imageUrl()
    {
        return Storage::url($this->image);
    }
}
