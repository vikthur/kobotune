<?php

namespace kobotune;

use Illuminate\Database\Eloquent\Model;

class Artist extends Model
{
    public function music(){

    	return $this->hasMany('kobotune\Music');

    }
}
