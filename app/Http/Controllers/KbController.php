<?php

namespace kobotune\Http\Controllers;

use kobotune\Music;
use kobotune\Artist;
use kobotune\Contact;
use Illuminate\Http\Request;
use kobotune\Mail\NewContact;
use kobotune\MailSubscription;
use Illuminate\Support\Facades\Mail;
use kobotune\Playlist;

class KbController extends Controller
{
	public function welcome()
	{

		$musics = Music::limit(12)->get();
		$artist = Artist::latest()->first();
		// dd($artist);
		return view('welcome', compact('musics', 'artist'));
	}

	public function contactUs(Request $request)
	{

		$contact = new Contact();

		$contact->name = $request->name;
		$contact->email = $request->email;
		$contact->subject = $request->subject;
		$contact->message = $request->message;


		// $contact->save();

		Mail::send(new NewContact($contact));

		$request->session()->flash('success', 'Thank You for contacting kobotune, we will reach out to you shortly');

		return back();
	}

	public function gallery()
	{

		$artists = Artist::simplePaginate(12);

		return view('pages.gallery', compact('artists'));
	}

	public function getArtist($name)
	{

		$artist = Artist::where('stage_name', $name)->first();

		return view('pages.artistPage', compact('artist'));
	}

	public function playlists()
	{

		$playlists = Playlist::simplePaginate(24);

		return view('pages.playlist', compact('playlists'));
	}

	public function subscribe(Request $request)
	{
		$subscribe = new MailSubscription();
		$subscribe->mail = $request->email;
		$subscribe->save();
		$request->session()->flash('success', 'Thank you for subscribing to kobotune!');
		return back();
	}
}
