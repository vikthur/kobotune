<?php

namespace kobotune\Http\Controllers;

use Auth;
use kobotune\Music;
use kobotune\Artist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use kobotune\Playlist;

class KobotuneController extends Controller
{
	public function __construct()
	{

		$this->middleware('auth');
	}

	public function index()
	{

		$artists = Artist::all();
		$playlists = Playlist::all();

		return view('admin.index', compact('artists', 'playlists'));
	}

	public function newArtist(Request $request)
	{

		$this->validate($request, [
			'name' => 'required',
			'stage_name' => 'required',
			'music_type' => 'required',
			'bio' => 'required',
			'image' => 'required',
		]);

		$artist = new Artist();

		$artist->name = $request->name;
		$artist->stage_name = $request->stage_name;
		$artist->music_type = $request->music_type;
		$artist->bio = $request->bio;

		// dd($request->file('image'));
		// $ext = $request->image->getClientOriginalExtension();
		$imgName = $request->image->getClientOriginalName();

		$store = Storage::disk('public')->putFile('artists/images', $request->file('image'));
		// dd($store);
		// $store = $request->file('image')->store(public_path('files/artists/images/').$imgName);
		$artist->image = $store;

		$artist->save();

		$request->session()->flash('success', 'successful!');

		return back();
	}

	public function newMusic(Request $request)
	{

		$this->validate($request, [

			'name' => 'required',
			'image' => 'required',
			'music_link' => 'required',
			'artist_id' => 'required',

		]);

		$music = new Music();

		$music->music_name = $request->name;
		$music->music_link = $request->music_link;
		$music->artist_id = $request->artist_id;

		$store = Storage::disk('public')->putFile('artists/music', $request->file('image'));
		// dd($store);
		$music->music_art = $store;

		$music->save();

		$request->session()->flash('success', 'successful!');

		return back();
	}

	public function getArtist($id)
	{

		$artist = Artist::findOrFail($id);

		return view('admin.artist', compact('artist'));
	}

	public function deteleMusic($id)
	{

		$music = Music::findOrFail($id);

		$music->delete();

		Session::flash('deteted', 'deteted');

		return back();
	}
}
