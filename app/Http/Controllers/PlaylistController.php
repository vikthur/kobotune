<?php

namespace kobotune\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use kobotune\Playlist;

class PlaylistController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:playlists',
            'music_type' => 'required',
            'url' => 'required|url',
            'image' => 'required|image'
        ]);

        $playlist = new Playlist();

        $playlist->name = $request->name;
        $playlist->genre = $request->music_type;

        $path = Storage::disk('public')->store('playlists', $request->file('image'));
        $playlist->image = $path;

        $playlist->url = $request->url;

        $playlist->save();

        session()->flash('success', 'Playlist saved successfully');
        return redirect()->back();
    }
}
