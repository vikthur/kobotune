<?php

namespace kobotune;

use Illuminate\Database\Eloquent\Model;

class Music extends Model
{
    public function artist(){

    	return $this->belongsTo('kobotune\Artist');
    	
    }
}
