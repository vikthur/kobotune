@extends('layouts.index')

@section('title')

{{$artist->name}}

@endsection


@section('content')

@if(Session::has('success'))
    <div class="alert alert-success">
      New Artist Added Successfully!.
    </div>
@endif

@if(Session::has('deteted'))
    <div class="alert alert-danger">
      Song deleted Successfully!.
    </div>
@endif

<div class="row" style="margin-left: 50px;">
    <a href="#" data-toggle="modal" data-target="#AddNewArtistSongModal" class="col-xl-3 col-sm-6 mb-3">
      <div class="card text-white bg-primary o-hidden h-100">
        <div class="card-body">
          <div class="card-body-icon">
            {{-- <i class="fas fa-fw fa-comments"></i> --}}
          </div>
          <div class="mr-5 text-center h4">Add New Song</div>
        </div>
      </div>
    </a>
    <div class="col-xl-3 col-sm-6 mb-3">
      <div class="card text-white bg-success o-hidden h-100">
        <div class="card-body">
          <div class="card-body-icon">
            {{-- <i class="fas fa-fw fa-shopping-cart"></i> --}}
          </div>
          @if($artist->music)
          <div class="mr-5 text-center h4">{{count($artist->music)}}</div>
          @else
          <div class="mr-5 text-center h4">0</div>
          @endif
        </div>
        <a class="card-footer text-white clearfix small z-1" href="#">
          <span class="float-left">Total Music</span>
          <span class="float-right">
            <i class="fas fa-angle-right"></i>
          </span>
        </a>
      </div>
    </div>
  </div>

  <!-- DataTables Example -->
          @if($artist)
          <div class="card mb-3">
            <div class="card-header">
              <i class="fas fa-table"></i>
              {{$artist->stage_name}} Songs</div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Song Title</th>
                      <th>Music Link</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Song Title</th>
                      <th>Music Link</th>
                      <th>Action</th>
                    </tr>
                  </tfoot>
                  <tbody>
                    	@foreach($artist->music as $music)
                    <tr>
                      <td>{{$music->music_name}}</a></td>
                      <td><a href="{{$music->music_link}}">{{$music->music_link}}</a></td>
                      <td>
                      	<form action="{{route('deleteMusic', $music->id)}}" method="post">
                      		@csrf
                      		@method('delete')
                      		<input type="hidden" name="id" value="{{$music->id}}">
                      		<button type="submit" class="text-capitalize btn btn-danger">delete</button>
                      	</form>
                      </td>
                    </tr>
                      	@endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          @endif

  <!-- Add new artist Modal -->
<div class="modal fade" id="AddNewArtistSongModal" tabindex="-1" role="dialog" aria-labelledby="AddNewArtistSongModal" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="AddNewArtistSongModal">Add New Artist</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="login-form">
          <form action="{{route('newMusic')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="name">Song Title</label>
                <input type="text" name="name" class="form-control" id="name" placeholder="Song title" required>
            </div>
            <div class="form-group">
                <label for="image">Song Art/image</label>
                <input type="file" class="form-control" id="image" name="image" accept="image/*" required>
            </div>
            <div class="form-group">
                <label for="music_link">Music Link</label>
                <input type="text" name="music_link" class="form-control" id="music_link" placeholder="Music Link eg http://www.soundcloud.com/muisicname" required>
            </div>
            <input type="hidden" name="artist_id" value="{{$artist->id}}">
            <div class="form-group">
              <button type="submit" class="btn btn-info oneMusic-btn mt-30 text-right">Submit</button>
            </div>
          </form>
      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

@endsection