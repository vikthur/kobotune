@extends('layouts.index')


@section('title')

{{"Dashboard"}}

@endsection

@section('content')

<div class="container-fluid">
	<!-- Breadcrumbs-->
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
			<a href="#">Dashboard</a>
		</li>
		<li class="breadcrumb-item active">Overview</li>
	</ol>

	@if(Session::has('success'))
		<div class="alert alert-success">
			{{session('success')}}
		</div>
	@endif

	@if($errors->any())
	@foreach ($errors->all() as $error)
		<div class="alert alert-danger">
			{{$error}}
		</div>
	@endforeach
	@endif

	<!-- Icon Cards-->
	<div class="row">
		<a href="#" data-toggle="modal" data-target="#AddNewArtistModal" class="col-xl-3 col-sm-6 mb-3">
			<div class="card text-white bg-primary o-hidden h-100">
				<div class="card-body">
					<div class="card-body-icon">
						{{-- <i class="fas fa-fw fa-comments"></i> --}}
					</div>
					<div class="mr-5 text-center h4">Add New Artist</div>
				</div>
			</div>
		</a>
		<a href="#" data-toggle="modal" data-target="#AddNewPlaylistModal" class="col-xl-3 col-sm-6 mb-3">
			<div class="card text-white bg-warning o-hidden h-100">
				<div class="card-body">
					<div class="card-body-icon">
						<i class="fas fa-fw fa-list"></i>
					</div>
					<div class="mr-5 text-center h4">Add Playlist</div>
				</div>
			</div>
		</a>
		<div class="col-xl-3 col-sm-6 mb-3">
			<div class="card text-white bg-success o-hidden h-100">
				<div class="card-body">
					<div class="card-body-icon">
						{{-- <i class="fas fa-fw fa-shopping-cart"></i> --}}
					</div>
					<div class="mr-5 text-center h4">690</div>
				</div>
				<a class="card-footer text-white clearfix small z-1" href="#">
					<span class="float-left">Total Audio</span>
					<span class="float-right">
						<i class="fas fa-angle-right"></i>
					</span>
				</a>
			</div>
		</div>
		<div class="col-xl-3 col-sm-6 mb-3">
			<div class="card text-white bg-danger o-hidden h-100">
				<div class="card-body">
					<div class="card-body-icon">
						<i class="fas fa-fw fa-life-ring"></i>
					</div>
					<div class="mr-5 text-center h4">479</div>
				</div>
				<a class="card-footer text-white clearfix small z-1" href="#">
					<span class="float-left">Total Video</span>
					<span class="float-right">
						<i class="fas fa-angle-right"></i>
					</span>
				</a>
			</div>
		</div>
	</div>
	<!-- playlists -->
	@if($playlists)
	<div class="card mb-3">
		<div class="card-header">
			<i class="fas fa-table"></i>
			Kobotune Playlists
		</div>
		<div class="card-body">
			<div class="table-responsive">
				<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th>Playlist Name</th>
							<th>Playlist Genre</th>
							<th>Playlist Image</th>
							<th>Playlist URL</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th>Playlist Name</th>
							<th>Playlist Genre</th>
							<th>Playlist Image</th>
							<th>Playlist URL</th>
						</tr>
					</tfoot>
					<tbody>
						@foreach($playlists as $playlist)
						<tr>
							<td><a href="#">{{$playlist->name}}</a></td>
							<td>{{$playlist->genre}}</td>
							<td><img src="{{$playlist->imageUrl()}}" width="50px" height="50px" alt="{{$playlist->name}}"></td>
							<td>{{$playlist->url}}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
	@endif
	{{-- artists --}}
	@if($artists)
	<div class="card mb-3">
		<div class="card-header">
			<i class="fas fa-table"></i>
			Kobotune Artists
		</div>
		<div class="card-body">
			<div class="table-responsive">
				<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th>Name</th>
							<th>Stage Name</th>
							<th>Music Type</th>
							<th>Music Count</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th>Name</th>
							<th>Stage Name</th>
							<th>Music Type</th>
							<th>Music Count</th>
						</tr>
					</tfoot>
					<tbody>
							@foreach($artists as $artist)
						<tr>
							<td><a href="/kobotune_lounge/artists/{{$artist->id}}">{{$artist->name}}</a></td>
							<td>{{$artist->stage_name}}</td>
							<td>{{$artist->music_type}}</td>
							@if($artist->music)
							<td>{{count($artist->music)}}</td>
							@else
							<td>0</td>
							@endif
						</tr>
								@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
	@endif

</div>
<!-- /.container-fluid -->

<!-- Add new artist Modal -->
<div class="modal fade" id="AddNewArtistModal" tabindex="-1" role="dialog" aria-labelledby="AddNewArtistModal" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="AddNewArtistModal">Add New Artist</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="login-form">
					<form action="{{route('newArtist')}}" method="post" enctype="multipart/form-data">
						@csrf
						<div class="form-group">
								<label for="name">Artist Name</label>
								<input type="text" name="name" class="form-control" id="name" placeholder="Artist Full Name" required>
						</div>
						<div class="form-group">
								<label for="stage_name">Artist Stage Name</label>
								<input type="text" name="stage_name" class="form-control" id="stage_name" placeholder="Artist Stage Name" required>
						</div>
						<div class="form-group">
								<label for="image">Artist Image</label>
								<input type="file" class="form-control" id="image" name="image" accept="image/*" required>
						</div>
						<div class="form-group">
								<label for="music_type">Artist Music Type</label>
								<input type="text" name="music_type" class="form-control" id="music_type" placeholder="Artist Music Type" required>
						</div>
						<div class="form-group">
								<label for="bio">Artist Little Bio</label>
								<textarea class="form-control" name="bio" id="bio" required></textarea>
						</div>
						<div class="form-group">
							<button type="submit" class="btn btn-info oneMusic-btn mt-30 text-right">Submit</button>
						</div>
					</form>
			</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>


				<!-- Add new artist Modal -->
<div class="modal fade" id="AddNewPlaylistModal" tabindex="-1" role="dialog" aria-labelledby="AddNewPlaylistModal" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="AddNewArtistModal">Add New Playlist</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="login-form">
					<form action="{{route('playlist.store')}}" method="post" enctype="multipart/form-data">
						@csrf
						<div class="form-group">
							<label for="name">Playlist Name</label>
							<input type="text" name="name" class="form-control" id="name" placeholder="Playlist Full Name" required>
						</div>
						<div class="form-group">
							<label for="music_type">Playlist Music Genre</label>
							<input type="text" name="music_type" class="form-control" id="music_type" placeholder="Playlist Music Genre" required>
						</div>
						<div class="form-group">
							<label for="url">Playlist URL</label>
							<input type="text" class="form-control" id="url" name="url" placeholder="https://open.spotify.com/playlist/37i9dQZF1DWYkaDif7Ztbp" required>
						</div>
						<div class="form-group">
							<label for="image">Playlist Image</label>
							<input type="file" class="form-control" id="image" name="image" accept="image/*" required>
						</div>
						<div class="form-group mt-5 text-right">
							<button type="submit" class="btn btn-info oneMusic-btn mt-30 text-right">Submit</button>
						</div>
					</form>
			</div>
			</div>
		</div>
	</div>
</div>

@endsection