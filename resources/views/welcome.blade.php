@extends('layouts.app')


@section('title')

    {{'Grooming the Natural Talent'}}

@endsection

@section('content')
    <!-- ##### Hero Area Start ##### -->
    <section class="hero-area">
        <div class="hero-slides owl-carousel">
            <!-- Single Hero Slide -->
            <div class="single-hero-slide d-flex align-items-center justify-content-center">
                <!-- Slide Img -->
                <div class="slide-img bg-img" style="background-image: url(oneMusic/img/bg-img/bg-1.jpg);"></div>
                <!-- Slide Content -->
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="hero-slides-content text-center">
                                {{-- <h6 data-animation="fadeInUp" data-delay="100ms">Our Values</h6> --}}
                                <h2 data-animation="fadeInUp" data-delay="300ms">Welcome to Kobotune <span>Welcome to Kobotune</span></h2>
                                <a data-animation="fadeInUp" data-delay="500ms" href="#welcome" class="btn oneMusic-btn mt-50">Learn More<i class="fa fa-angle-double-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Single Hero Slide -->
            <div class="single-hero-slide d-flex align-items-center justify-content-center">
                <!-- Slide Img -->
                <div class="slide-img bg-img" style="background-image: url(oneMusic/img/bg-img/bg-2.jpg);"></div>
                <!-- Slide Content -->
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="hero-slides-content text-center">
                                <h6 data-animation="fadeInUp" data-delay="100ms">Our Services</h6>
                                <h2 data-animation="fadeInUp" data-delay="300ms">digital distribution, Music Licencing and much more <span>digital distribution, Music Licencing and Much More</span></h2>
                                <a data-animation="fadeInUp" data-delay="500ms" href="#services" class="btn oneMusic-btn mt-50">Know More <i class="fa fa-angle-double-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            

            <!-- Single Hero Slide -->
            <div class="single-hero-slide d-flex align-items-center justify-content-center">
            <div class="slide-img bg-img" style="background-image: url(oneMusic/img/bg-img/bg-3.jpg);"></div>
                <!-- Slide Content -->
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="hero-slides-content text-center">
                                {{-- <h6 data-animation="fadeInUp" data-delay="100ms">Latest album</h6> --}}
                                <h2 data-animation="fadeInUp" data-delay="300ms">Get Heard and get Paid <span>Get Heard and get Paid</span></h2>
                                <a data-animation="fadeInUp" data-delay="500ms" href="/contactUs" class="btn oneMusic-btn mt-50">Contact Us<i class="fa fa-angle-double-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Single Hero Slide -->
            <div class="single-hero-slide d-flex align-items-center justify-content-center">
                <!-- Slide Img -->
                <div class="slide-img bg-img" style="background-image: url(oneMusic/img/bg-img/bg-4.jpg);"></div>
                <!-- Slide Content -->
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="hero-slides-content text-center">
                                <h6 data-animation="fadeInUp" data-delay="100ms">Our Values</h6>
                                <h2 data-animation="fadeInUp" data-delay="300ms">Empathy, Craftmanship and Sensibility <span>Empathy, Craftmanship and Sensibility</span></h2>
                                <a data-animation="fadeInUp" data-delay="500ms" href="/about" class="btn oneMusic-btn mt-50">Know More <i class="fa fa-angle-double-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ##### Hero Area End ##### -->

    <!-- ##### Latest Albums Area Start ##### -->
    <section class="latest-albums-area section-padding-100" id="welcome">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-heading style-2">
                        {{-- <p>See what’s new</p> --}}
                        <h2>NO LABEL? NO PROBLEM.</h2>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-12 col-lg-9">
                    <div class="ablums-text text-center mb-70">
                        <p>Our services work for you.
                        Whether you’re an artist, songwriter, musician, label or music publisher, kobotune's full range of services offers a modern alternative to the
                        traditional music industry.
                        </p>
                        <p>
                            <blockquote>
                                We provide you all the digital tools you need to maximize your potential because your music deserve everything and you deserve a music company that acts as partner, not as a provider.
                            </blockquote>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ##### Latest Albums Area End ##### -->

    <!-- ##### Our Services Area Start ##### -->
        <section class="latest-albums-area section-padding-100" id="services">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="section-heading style-2">
                            {{-- <p>See what’s new</p> --}}
                            <h2 class="text-capitalize">Our Services</h2>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-md-4 text-center">
                        <img src="{{asset('img/1.jpg')}}" class="img-fluid">
                        <h4 class="text-capitalize text-center">digital distribution</h4>
                        <p class="text-center">
                            We distribute and monetize all your release on all digital platforms like spotify, iTunes, tidal, Deezer, soundcloud and so much more
                        </p>
                    </div>
                    <div class="col-md-4 text-center">
                        <img src="{{asset('img/2.jpg')}}" class="img-fluid">
                        <h4 class="text-capitalize text-center">Release Strategy</h4>
                        <p class="text-center">
                            We work out the best release plan for your music together 
                        </p>
                    </div>
                    <div class="col-md-4 text-center">
                        <img src="{{asset('img/3.jpg')}}" class="img-fluid">
                        <h4 class="text-capitalize text-center">digital marketing and promotion</h4>
                        <p class="text-center">
                            We pitch your songs to playlist on every music platforms where you get a chance to be heard by news fans from all over the world and we promote all your release on every media platforms available. 
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <!-- ##### Our Services Area End ##### -->

    <!-- ##### Buy Now Area Start ##### -->
    <section class="oneMusic-buy-now-area has-fluid bg-gray section-padding-100">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="section-heading style-2">
                        <p>See what’s new</p>
                        <h2>Fresh Out From Kobotune</h2>
                    </div>
                </div>
            </div>

            <div class="row">
                @if($musics && count($musics) > 0)
                @foreach($musics as $music)
                <!-- Single Album Area -->
                <div class="col-12 col-sm-6 col-md-4 col-lg-2">
                    <a href="{{$music->music_link}}" class="single-album-area wow fadeInUp" data-wow-delay="200ms">
                        <div class="album-thumb">
                            <img src="{{'/files/'.$music->music_art}}" alt="Music Art">
                        </div>
                        <div class="album-info">
                            {{-- <a href="#"> --}}
                                <h5>{{$music->music_name}}</h5>
                            {{-- </a> --}}
                            <p>{{$music->artist->name}}</p>
                        </div>
                    </a>
                </div>
                @endforeach
                @else
                <div class="col-12 col-sm-6 col-md-4 col-lg-2">
                    <p class="text-center">Sorry No Music Yet :)</p>
                </div>
                @endif
            </div>
            @if($musics && count($musics) > 0)
            <div class="row">
                <div class="col-12">
                    <div class="load-more-btn text-center wow fadeInUp" data-wow-delay="300ms">
                        <a href="/music" class="btn oneMusic-btn">More <i class="fa fa-angle-double-right"></i></a>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </section>
    <!-- ##### Buy Now Area End ##### -->

        <!-- ##### Buy Now Area Start ##### -->
    <section class="oneMusic-buy-now-area has-fluid bg-gray section-padding-100">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="section-heading style-2">
                        {{-- <p>See what’s new</p> --}}
                        <h2>Frequently Asked Questions (FAQs)</h2>
                    </div>
                </div>
            </div>

            <div class="row col-md-12">
                <div id="accordion" style="width: inherit;">
                  <div class="card col-md-12">
                    <div class="card-header bg-dark" id="headingOne">
                      <h5 class="mb-0">
                        <button class="btn btn-link text-uppercase text-white" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                          Why choose KoboTune?
                        </button>
                      </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                      <div class="card-body">
                        <p>
                        If you’re an independent artist making music, you’re in the right place.</p>
                        <p>
                        We offer a simple yet effective way to release your music – and take it further.</p>

                        <h4>We give you flexibility</h4>
                        <p>
                        With KoboTune there’s no more confusion about how to get your music on major digital music services like Spotify and Apple Music – we do it for you quickly and easily. And you can even choose to pre-schedule releases weeks ahead of time.
                        You also have the freedom to take down your releases at any time.</p>
                      </div>
                    </div>
                  </div>
                  <div class="card col-md-12">
                    <div class="card-header bg-dark" id="headingTwo">
                      <h5 class="mb-0">
                        <button class="btn btn-link collapsed text-white text-uppercase" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                          What is music distribution and how does it work?
                        </button>
                      </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                      <div class="card-body">
                        <p>
                        Distribution is a crucial part of promoting your music as an artist – and helps you reach fans.</p>
                        <p>
                        Brick and mortar music distributors were once the only way for record labels and independent artists to get their music – records and CDs – in the hands of listeners. But digital distribution changed all that for the better.</p>

                        <p><strong>How digital distribution works</strong></p>
                        <p>
                        Streaming and digital platforms are the new record shops. But music distribution no longer takes weeks and months. Today, digital distribution quickly gets your music on Spotify, Apple Music, Google Play and other major music services. And you make money – royalties – every time someone streams, downloads or buys your music.</p>

                        <h2 class="text-center">The benefits</h2>

                        <h4>Digital distribution</h4>
                        <ul class="list-group">
                            <li class="list-group-item">
                                Releases your music to the world quickly
                            </li>
                            <li class="list-group-item">
                                Gets your music into as many ears as possible
                            </li>
                            <li class="list-group-item">
                                Boosts your profile to potential fans worldwide
                            </li>
                            <li class="list-group-item">
                                Gives you credibility – features your music alongside superstars like Kanye and Katy Perry
                            </li>
                            <li class="list-group-item">
                                Lets you keep the rights to your music
                            </li>
                            <li class="list-group-item">
                                Helps you get paid for your work
                            </li>
                            <li class="list-group-item">
                                Gives you stats to measure your success
                            </li>
                            <li class="list-group-item">
                                Lets you plan better for the future
                            </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div class="card col-md-12">
                    <div class="card-header bg-dark" id="headingThree">
                      <h5 class="mb-0">
                        <button class="btn btn-link collapsed text-uppercase text-white" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                          Can I make money as an unsigned artist?
                        </button>
                      </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                      <div class="card-body">
                        <p>
                        Most importantly, you need several quality, completed songs. So partial lyrics in your head or half-finished tracks on your hard drive don’t count. While there’s no set way to make money from your music as an unsigned artist, there are a few things you can do to get you on your way:</p>

                        <h4>Get attention</h4>
                        <p>
                        Don’t keep those amazing tracks to yourself. Once you have a few finished top songs, share them and build connections via socials on YouTube, Instagram, Facebook and Twitter. Doing this allows you to reach a broader audience and can potentially attract the attention of organisations willing to pay for featuring your music.</p>

                        <h4>Distribute your music</h4>

                        <p>Distribution is a crucial part of promoting your music.  iTunes, Spotify, Amazon Unlimited are just some of the streaming platforms and digital music stores you can get on. Distributing your music and content on platforms like Spotify and Apple Music can help attract attention from new fans and the music industry alike. You make money every time people stream, download or buy your music on these platforms.</p>

                        <h4>Making money from streaming</h4>

                        <p>The amount of money you can make from streaming varies according to how often your music is streamed each month, the number of subscriptions and ad revenue. When someone streams your music you’ll be paid a proportionate share of either the subscription revenue – or in the case of Spotify’s free-tier, a proportion of the monthly ad revenue.</p>

                      </div>
                    </div>
                  </div>
                  <div class="card col-md-12">
                    <div class="card-header bg-dark" id="headingFour">
                      <h5 class="mb-0">
                        <button class="btn btn-link collapsed text-uppercase text-center text-white" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                          What’s the best way to promote myself?
                        </button>
                      </h5>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                      <div class="card-body">
                        <p>
                        Today, labels increasingly look for self-sufficient, multi-talented artists who have growing fanbases and can create a buzz using unique branding.</p>
                        <p>
                        So besides making mind-blowing music, you need to be able to promote yourself as an artist.</p>
                        <p>
                        Yes, promo takes time, effort and planning – but don’t panic. You’ve got this! In fact, you can rely on that same drive, determination and vision you used to create your music to masterfully market yourself too.</p>
                        <p>
                        But self-promotion is not one-size fits all. It should reflect your unique personality and brand. So before you even begin, consider how you want to promote yourself in a way that feels natural and authentic.</p>
                        <p>
                        YouTube, Facebook, and Instagram are just some of the ways you can plug yourself on social media, so also consider which ones work best for you.</p>
                        <p>
                        Importantly, promoting yourself should never feel forced. Whatever you decide, always promote your new music and gigs – and build connections with your fans.</p>

                        <h4>Get our free self-promotion guide</h4>

                        <p>Our free guide is brimming with top tips on how to promote yourself and includes advice on:</p>
                        <ul class="list-group">
                            <li class="list-group-item">Finding your own voice and promoting in an authentic way</li>
                            <li class="list-group-item">The best self-promotions books, articles and podcasts</li>
                            <li class="list-group-item">Building an email list</li>
                            <li class="list-group-item">Creating an EPK (that’s an Electronic Press Kit)</li>
                            <li class="list-group-item">Using social media as your self-promo machine.</li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ##### Buy Now Area End ##### -->

    <section class="mt-5 bg-dark">
        <div class="container">
            <div class="row justify-content-center py-5">
                <div class="col-lg-12 text-center text-white">Subscribe To Our Newsletter</div>
                <form class="col-md-6" method="post" action="/subscribe">
                    @csrf
                    <div class="form-group">
                        <input type="email" name="email" placeholder="your email address" class="form-control" style="background-color: transparent; padding: 25px; border-radius: 20px">
                    </div>
                    <div class="form-group text-center">
                        <button type="submit" class="btn oneMusic-btn">Subscribe</button>
                    </div>
                </form>
                @if(Session::has('success'))
                    <div class="text-white">Thank You for subscribing to kobotune Newsletter</div>
                @endif
            </div>
        </div>
    </section>

    <!-- ##### Featured Artist Area Start ##### -->
    <section class="featured-artist-area section-padding-100 bg-img bg-overlay bg-fixed" style="background-image: url(oneMusic/img/bg-img/bg-4.jpg);">
        <div class="container">
            @if($artist)
            <div class="row align-items-end">
                <div class="col-12 col-md-5 col-lg-4">
                    <div class="featured-artist-thumb">
                        <img src="{{'files/'.$artist->image}}" alt="">
                    </div>
                </div>
                <div class="col-12 col-md-7 col-lg-8">
                    <div class="featured-artist-content">
                        <!-- Section Heading -->
                        <div class="section-heading white text-left mb-30">
                            <p>Featured artist</p>
                            <h2>{{$artist->stage_name}}</h2>
                        </div>
                        <p>{{$artist->bio}}</p>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </section>
    <!-- ##### Featured Artist Area End ##### -->
@endsection