@extends('layouts.app')

@section('title')

    {{'Music'}}

@endsection

@section('content')
    <!-- ##### Breadcumb Area Start ##### -->
    <section class="breadcumb-area bg-img bg-overlay" style="background-image: url(oneMusic/img/bg-img/breadcumb3.jpg);">
        <div class="bradcumbContent">
            <p>Our Artists</p>
            <h2>Kobotune Artists</h2>
        </div>
    </section>
    <!-- ##### Breadcumb Area End ##### -->

    <!-- ##### Album Catagory Area Start ##### -->
    <section class="album-catagory section-padding-100-0">
        <div class="container">
            <div class="row oneMusic-albums">
                @if($artists)
                @foreach($artists as $artist)
                <!-- Single Album -->
                <div class="col-12 col-sm-4 col-md-3 col-lg-2 single-album-item t c p">
                    <div class="single-album">
                        <img src="{{'files/'.$artist->image}}" alt="">
                        <div class="album-info">
                            <a href="/artistPage/{{$artist->stage_name}}">
                                <h5>{{$artist->name}}</h5>
                            </a>
                        </div>
                    </div>
                </div>
                @endforeach
                @endif
            </div>
        </div>
    </section>
    <!-- ##### Album Catagory Area End ##### -->
@endsection