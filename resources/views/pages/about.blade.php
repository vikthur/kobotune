@extends('layouts.app')

@section('title')

    {{'About Us'}}

@endsection

@section('content')
<section class="breadcumb-area bg-img bg-overlay" style="background-image: url(oneMusic/img/bg-img/breadcumb3.jpg);">
    <div class="bradcumbContent">
        <p>About Us</p>
        <h2>What is Kobotune</h2>
    </div>
</section>

<section class="album-catagory section-padding-100-0">
	<div class="container">
	    <div class="row justify-content-center">
	        <div class="col-12 col-lg-9">
	            <div class="ablums-text text-center mb-70">
	                <p>
	                	KOBOTUNE is a new kind of music company. Our mission is clear; to help artists and songwriters make the very most of their songs and recordings in the digital age.
	                </p>
	                <p>
	                	Whether you’re a budding new artist, an accomplished musician or an independent record label, we’ve got you!
	                </p>
	                <p>
	                	And as a new music company we know that songwriters and artists don’t work for us – we work for them. It’s a knowledge as clear as our commitment; to make KOBOTUNE the best international music company – both for songwriters, artists and those who want to use their music.
	                </p>
	                <p>
	                	We are a new kind of music company for a new world.
						A new world in which service to artists and writers is key,our purpose is clear - to support your career and maximize your income.

	                </p>
	                <p>
	                	Unique to our clients, we offer transparent and unlimited access to our online portal, providing real-time worldwide sales and streaming data from our partners.
	                </p>
	            </div>
	        </div>
	    </div>
	</div>
</section>

@endsection