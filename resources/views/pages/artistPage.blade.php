@extends('layouts.app')

@section('title')

    {{$artist->stage_name}}

@endsection

@section('content')
    <!-- ##### Breadcumb Area Start ##### -->
    <section class="breadcumb-area bg-img bg-overlay" style="background-image: url({{asset('oneMusic/img/bg-img/breadcumb3.jpg')}});">
        <div class="bradcumbContent">
            <p>Our Artist</p>
            <h2>{{($artist->name)}}</h2>
            <p>{{$artist->bio}}</p>
        </div>
    </section>
    <!-- ##### Breadcumb Area End ##### -->

    <!-- ##### Album Catagory Area Start ##### -->
    <section class="album-catagory section-padding-100-0">
        <div class="container">
            <div class="row oneMusic-albums">
                @if($artist->music)
                @foreach($artist->music as $music)
                <!-- Single Album -->
                <div class="col-12 col-sm-4 col-md-3 col-lg-2 single-album-item t c p">
                    <div class="single-album">
                        <img src="{{'files/'.$music->music_art}}" alt="">
                        <div class="album-info">
                            <a href="{{$music->stage_name}}">
                                <h5>{{$music->music_name}}</h5>
                            </a>
                        </div>
                    </div>
                </div>
                @endforeach
                @endif
            </div>
        </div>
    </section>
    <!-- ##### Album Catagory Area End ##### -->
@endsection