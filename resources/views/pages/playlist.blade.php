@extends('layouts.app')

@section('title')

    {{'Music'}}

@endsection

@section('content')
    <!-- ##### Breadcumb Area Start ##### -->
    <section class="breadcumb-area bg-img bg-overlay" style="background-image: url(oneMusic/img/bg-img/breadcumb3.jpg);">
        <div class="bradcumbContent">
            <p>Enjoy our playlists</p>
            <h2>Kobotune Playlists</h2>
        </div>
    </section>
    <!-- ##### Breadcumb Area End ##### -->

    <!-- ##### Album Catagory Area Start ##### -->
    {{-- <section class="album-catagory section-padding-100-0">
        <div class="container">
            <div class="row oneMusic-albums">
                @if($playlists)
                @foreach($playlists as $playlist)
                <div class="col-md-4 text-center">
                        <img src="{{$playlist->imageUrl()}}" class="img-fluid">
                        <h4 class="text-capitalize text-center">{{$playlist->name}}</h4>
                        <p class="text-center">
                            We distribute and monetize all your release on all digital platforms like spotify, iTunes, tidal, Deezer, soundcloud and so much more
                        </p>
                    </div>
                @endforeach
                @endif
            </div>
        </div>
    </section> --}}
    <!-- ##### Album Catagory Area End ##### -->

    <section class="latest-albums-area section-padding-100" id="services">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="section-heading style-2">
                            {{-- <p>See what’s new</p> --}}
                            <h2 class="text-capitalize">Latest Playlists</h2>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    @if($playlists)
                    @foreach($playlists as $playlist)
                    <div class="col-md-4 text-center">
                        <a href="{{$playlist->url}}" target="blank">
                            <img src="{{$playlist->imageUrl()}}" width="300px" height="300px" class="img-fluid">
                            <h4 class="text-capitalize text-center">{{$playlist->name}}</h4>
                            <p class="text-center">
                                Genre: {{$playlist->genre}}
                            </p>
                        </a>
                    </div>
                    @endforeach
                    @endif
                </div>
            </div>
        </section>

@endsection