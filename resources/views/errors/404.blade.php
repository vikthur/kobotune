<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>Kobotune - Not Found</title>

   	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

  </head>
  <body class="text-center bg-dark text-light">
    <div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">

	  <main role="main" class="inner cover">
	    <h1 class="cover-heading">Oops!</h1>
	    <p class="lead">Seems you are lost. The page you seek is not found on this server.</p>
	    <p class="lead">
	      <a href="/" class="btn btn-lg btn-secondary">Go Home</a>
	    </p>
	  </main>
</div>
</body>
</html>
