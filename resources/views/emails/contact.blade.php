<div style="display: flex;">
	<strong>Hello Admin!</strong>
	<p>
		There is a new contact on the website
	</p>
	<h4>contact details</h4>
	<ul style="list-style: none;">
		<li>
			<strong>Name: </strong> {{$contact->name}}
		</li>
		<li>
			<strong>Email: </strong> {{$contact->email}}
		</li>
		<li>
			<strong>Contact Subject: </strong> {{$contact->subject}}
		</li>
		<li>
			<strong>Contact Message: </strong> {{$contact->message}}
		</li>
	</ul>
	
</div>