<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'KbController@welcome');

Auth::routes();

// Route::get('/login', function(){
// 	return view('errors.404');
// })->name('login');

// Route::get('register', function(){
// 	return view('errors.404');
// });

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/playlists', 'KbController@playlists')->name('playlists');

Route::get('/gallery', 'KbController@gallery')->name('gallery');

Route::get('artistPage/{name}', 'KbController@getArtist');

Route::get('/contactUs', function () {
	return view('pages.contactUs');
})->name('contact');

Route::get('/about', function () {
	return view('pages.about');
})->name('about');

Route::post('/contactUs', 'KbController@contactUs')->name('contactUs');

Route::post('newArtist', 'KobotuneController@newArtist')->name('newArtist');

Route::post('/newMusic', 'KobotuneController@newMusic')->name('newMusic');

Route::get('/kobotune_lounge', 'KobotuneController@index');

Route::get('/kobotune_lounge/artists/{id}', 'KobotuneController@getArtist');

Route::delete('/kobotune_lounge/artists/{id}', 'KobotuneController@deteleMusic')->name('deleteMusic');

Route::post('/subscribe', 'KbController@subscribe');

Route::group(['prefix' => 'kobotune_lounge'], function () {
	Route::Resource('playlist', 'PlaylistController');
});
